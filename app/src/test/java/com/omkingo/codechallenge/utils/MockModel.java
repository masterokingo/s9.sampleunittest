package com.omkingo.codechallenge.utils;

import com.omkingo.codechallenge.service.model.AboutItem;
import com.omkingo.codechallenge.service.model.AboutModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;

/**
 * Created by omkingo on 11/23/17.
 */

public class MockModel{

    public static AboutModel newAboutModel(){
        Random random = new Random();
        AboutModel aboutModel = new AboutModel();

        AboutItem item = new AboutItem();
        item.setID(random.nextInt(10000));
        item.setAndroidVer("1.2.13");
        item.setDriverVer("1.2.13");
        item.setIOSVer("1.2.13");
        item.setFBURL("www.fb.com");
        item.setOperatorPhoneNoEnglish("08x-xxxxxxx");
        item.setOperatorPhoneNoThai("08x-xxxxxxx");
        item.setIGURL("www.ig.com");
        item.setWebURL("www.navigo.com");

        aboutModel.setItem(item);
        return aboutModel;
    }


}
