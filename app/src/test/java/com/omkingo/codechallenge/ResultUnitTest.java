package com.omkingo.codechallenge;

import com.omkingo.codechallenge.service.api.ApiEndpoint;
import com.omkingo.codechallenge.service.model.AboutModel;
import com.omkingo.codechallenge.utils.MockModel;
import com.omkingo.codechallenge.view.ui.main.result.ResultMvp;
import com.omkingo.codechallenge.view.ui.main.result.ResultPresenter;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ResultUnitTest {

    private ApiEndpoint apiEndpoint;
    private ResultMvp resultMap;
    private ResultPresenter resultPresenter;

    @BeforeClass
    public static void setupClass() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());
    }

    @Before
    public void setUp() {
        BaseApplication baseApplication = new BaseApplication();
        apiEndpoint = mock(ApiEndpoint.class);
        resultPresenter = new ResultPresenter();
        resultMap = mock(ResultMvp.class);
        when(resultMap.getContext()).thenReturn(baseApplication);

        resultPresenter.attachView(resultMap);
    }

    @After
    public void tearDown() {
        resultPresenter.detachView();
    }

    @Test
    public void textApiResponse() throws Exception {
        AboutModel aboutModels = MockModel.newAboutModel();

        when(apiEndpoint.getAboutNavigo()).thenReturn(Observable.just(aboutModels));

        resultPresenter.getAbout();
        verify(resultMap).showLoading();
        verify(resultMap).showMessage("Loading");

    }

    @Test
    public void textApiFailure() throws Exception {
        when(apiEndpoint.getAboutNavigo()).thenReturn(Observable.error(new RuntimeException("error")));

        resultPresenter.getAbout();
        verify(resultMap).showLoading();
        verify(resultMap).showMessage("No Data");

    }
}