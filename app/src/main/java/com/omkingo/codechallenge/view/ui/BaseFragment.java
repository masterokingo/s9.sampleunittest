package com.omkingo.codechallenge.view.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public abstract class BaseFragment extends Fragment{

    protected LayoutInflater mInflater;
    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getInflaterLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInflater = getActivity().getLayoutInflater();
        mView = view;

        bindView();
        setupInstance();
        setupView();
        initFragment();
    }

    protected View findViewById(int id) {
        return mView.findViewById(id);
    }

    protected abstract int getInflaterLayout();

    protected abstract void bindView();

    protected void setupInstance(){}

    protected void setupView(){}

    protected abstract void initFragment();
}
