package com.omkingo.codechallenge.view.ui.main.result;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.omkingo.codechallenge.R;
import com.omkingo.codechallenge.service.model.AboutModel;
import com.omkingo.codechallenge.view.ui.BaseFragment;

public class ResultFragment extends BaseFragment implements ResultMvp{

    private ProgressBar progress_bar;
    private TextView tv_msg;

    private ResultPresenter mPresenter;

    public static ResultFragment newInstance(){
        ResultFragment frag = new ResultFragment();

        Bundle args = new Bundle();
        frag.setArguments(args);

        return frag;
    }



    @Override
    protected int getInflaterLayout() {
        return R.layout.fragment_result;
    }

    @Override
    protected void bindView() {
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
    }

    @Override
    protected void initFragment() {
        mPresenter = new ResultPresenter();
        mPresenter.attachView(this);

        mPresenter.getAbout();
    }

    @Override
    public void showLoading() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void showUi(AboutModel aboutModel) {
        tv_msg.setText(aboutModel.getItem().toString());

        hideLoading();
    }

    @Override
    public void showMessage(String msg) {
        tv_msg.setText(msg);
    }

    @Override
    public Context getContext() {
        return getContext();
    }
}
