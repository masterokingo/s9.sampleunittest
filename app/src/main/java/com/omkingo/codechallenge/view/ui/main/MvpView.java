package com.omkingo.codechallenge.view.ui.main;

import android.content.Context;

public interface MvpView {
    Context getContext();
}
