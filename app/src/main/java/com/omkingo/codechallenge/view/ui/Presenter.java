package com.omkingo.codechallenge.view.ui;

public interface Presenter<V> {

    void attachView(V view);

    void detachView();

}
