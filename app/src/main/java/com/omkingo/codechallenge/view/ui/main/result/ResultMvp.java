package com.omkingo.codechallenge.view.ui.main.result;

import com.omkingo.codechallenge.service.model.AboutModel;
import com.omkingo.codechallenge.view.ui.main.MvpView;

public interface ResultMvp extends MvpView {

    void showLoading();

    void hideLoading();

    void showUi(AboutModel aboutModel);

    void showMessage(String msg);

}
