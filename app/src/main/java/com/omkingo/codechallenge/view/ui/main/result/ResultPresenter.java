package com.omkingo.codechallenge.view.ui.main.result;

import com.omkingo.codechallenge.service.api.ApiEndpoint;
import com.omkingo.codechallenge.service.api.ApiBuilder;
import com.omkingo.codechallenge.service.model.AboutModel;
import com.omkingo.codechallenge.view.ui.Presenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by omkingo on 11/23/17.
 */

public class ResultPresenter implements Presenter<ResultMvp> {

    private ResultMvp resultView;
    private ApiBuilder apiService;
    private Disposable diaposable;

    @Override
    public void attachView(ResultMvp view) {
        resultView = view;

        apiService = new ApiBuilder();
    }

    @Override
    public void detachView() {
        this.resultView = null;

        if (diaposable != null) diaposable.dispose();
    }

    public void getAbout(){
        resultView.showLoading();

        if (diaposable != null) diaposable.dispose();

        ApiEndpoint apiEndpoint = apiService.create();

        diaposable = apiEndpoint.getAboutNavigo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse, this::handleError);

        resultView.showMessage("Loading");
    }

    private void handleResponse(AboutModel aboutModel){
        System.out.println("Success");
        resultView.showUi(aboutModel);
    }

    private void handleError(Throwable error){
        resultView.showMessage("No Data");
        resultView.hideLoading();
    }
}
