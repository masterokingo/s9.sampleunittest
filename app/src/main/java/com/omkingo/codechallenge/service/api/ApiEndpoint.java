package com.omkingo.codechallenge.service.api;


import com.omkingo.codechallenge.service.model.AboutModel;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiEndpoint {

    @GET("AboutNavigo/GetAbout")
    Observable<AboutModel> getAboutNavigo();
}