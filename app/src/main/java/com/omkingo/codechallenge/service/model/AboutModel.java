package com.omkingo.codechallenge.service.model;

/**
 * Created by Mast3ro on 9/18/2015.
 */
public class AboutModel {

    /**
     * ID : 1
     * FBURL : https://www.facebook.com/NaviGoSamui
     * webURL : http://www.navigosamui.com
     * IGURL : https://instagram.com/NaviGo_Samui/
     * OperatorPhoneNoThai : +66617616666
     * OperatorPhoneNoEnglish : +66617616666
     * iOSVer : null
     * AndroidVer : 11260
     * UpdateDate : null
     */

    private String Status;
    private String Message;
    private String Param;

    public String getParam() {
        return Param;
    }

    public void setParam(String param) {
        Param = param;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getStatus() {
        return Status;
    }

    public String getMessage() {
        return Message;
    }

    private AboutItem Item;

    public void setItem(AboutItem Item) {
        this.Item = Item;
    }

    public AboutItem getItem() {
        return Item;
    }
}
