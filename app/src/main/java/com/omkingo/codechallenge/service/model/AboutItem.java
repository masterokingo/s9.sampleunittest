package com.omkingo.codechallenge.service.model;

/**
 * Created by Mast3ro on 9/18/2015.
 */
public class AboutItem {
    private int ID;
    private String FBURL;
    private String webURL;
    private String IGURL;
    private String OperatorPhoneNoThai;
    private String OperatorPhoneNoEnglish;
    private Object iOSVer;
    private String AndroidVer;
    private String DriverVer;
    private Object UpdateDate;

    private String PaymentGatewayAPIURL;

    public String getPaymentGatewayAPIURL() {
        return PaymentGatewayAPIURL;
    }

    public void setPaymentGatewayAPIURL(String paymentGatewayAPIURL) {
        PaymentGatewayAPIURL = paymentGatewayAPIURL;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setFBURL(String FBURL) {
        this.FBURL = FBURL;
    }

    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }

    public void setIGURL(String IGURL) {
        this.IGURL = IGURL;
    }

    public void setOperatorPhoneNoThai(String OperatorPhoneNoThai) {
        this.OperatorPhoneNoThai = OperatorPhoneNoThai;
    }

    public void setOperatorPhoneNoEnglish(String OperatorPhoneNoEnglish) {
        this.OperatorPhoneNoEnglish = OperatorPhoneNoEnglish;
    }

    public void setIOSVer(Object iOSVer) {
        this.iOSVer = iOSVer;
    }

    public void setAndroidVer(String AndroidVer) {
        this.AndroidVer = AndroidVer;
    }

    public void setUpdateDate(Object UpdateDate) {
        this.UpdateDate = UpdateDate;
    }

    public int getID() {
        return ID;
    }

    public String getFBURL() {
        return FBURL;
    }

    public String getWebURL() {
        return webURL;
    }

    public String getIGURL() {
        return IGURL;
    }

    public String getOperatorPhoneNoThai() {
        return OperatorPhoneNoThai;
    }

    public String getOperatorPhoneNoEnglish() {
        return OperatorPhoneNoEnglish;
    }

    public Object getIOSVer() {
        return iOSVer;
    }

    public String getAndroidVer() {
        return AndroidVer;
    }

    public String getDriverVer() {
        return DriverVer;
    }

    public void setDriverVer(String driverVer) {
        DriverVer = driverVer;
    }

    public Object getUpdateDate() {
        return UpdateDate;
    }

    @Override
    public String toString() {
        return "FbUrl: " + FBURL + "\n" +
                "WebUrl: " + webURL+ "\n" +
                "IGURL: " + IGURL+ "\n" +
                "OperatorPhoneNoThai: " + OperatorPhoneNoThai + "\n" +
                "OperatorPhoneNoEnglish: " + OperatorPhoneNoEnglish + "\n" +
                "iOSVer: " + iOSVer+ "\n" +
                "AndroidVer: " + AndroidVer
                ;
    }
}
