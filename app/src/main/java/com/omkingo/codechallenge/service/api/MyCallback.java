package com.omkingo.codechallenge.service.api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by omkingo on 11/15/17.
 */

public class MyCallback<T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
            onSuccess(response.body());
        }else {
            onFailed(response.code(), response.message(), response.raw().request().url().toString());
        }
    }

    public void onSuccess(T resObj){}

    public void onFailed(int code, String message, String url){}

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFailed(500, t.getMessage(), call.request().url().toString());
    }
}
