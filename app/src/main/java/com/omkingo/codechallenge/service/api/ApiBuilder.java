package com.omkingo.codechallenge.service.api;

import com.omkingo.codechallenge.service.api.converter.ToStringConverterFactory;
import com.omkingo.codechallenge.service.model.AboutModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiBuilder {

    private static final String NAVIGO_URL = "http://api2.navigo.run/api/";

    public ApiBuilder(){
    }

    public ApiEndpoint create() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NAVIGO_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(ApiEndpoint.class);
    }



}
